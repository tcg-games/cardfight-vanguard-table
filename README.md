# Cardfight Vanguard table

This is a fan made project for a Cardfight Vanguard table.

The 1.0 project will be a table just as in the series: To support the game that is being played on it.

# Feature requests

Feature: A new functionality for the Cardfight Vanguard table

Bug: An implemented functionality which does not function as it should be

1. Go to Issues > List
2. Top right: New Issue
3. Fill in the details of your idea as much as you can. 
   You do need a gitlab account to create an issue.

Soon: a feature and Bug template for the issues!